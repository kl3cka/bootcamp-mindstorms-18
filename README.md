# Workshop Mindstorms

## Einrichten der Umgebung
- Intellij
- Gitlab
- Beispiel bauen und Upload


## Bewegen des Roboters
- Vor und zurück
- Drehen

## Lichtsensor benutzen
- Werte ablesen und auf dem Display anzeigen

## Roboter folgt einer Linie
- Multithreading
- Kurven folgen
- Sound am Ende abspielen

## Challenge
Welcher Roboter ist der schnellste auf einem vorgegebenen Kurs?

## Doku:
https://roberta-home.de/sites/default/files/media/Roberta-EV3programmierenJava_small.pdf
http://www.lejos.org/ev3/docs/